// Punteros
#include <stdio.h>

void sumar(int *a, int *b);

int main(void) {
    int numero = 3;
    int *el_numero = &numero;
    printf("Numero: %d\n", numero);
    printf("Ubicacion en memoria %d\n", el_numero);
    printf("Numero via referencia %d\n", *el_numero);

    int a = 1;
    int b = 10;
    sumar(&a, &b);
    printf("Sumados: %d %d", a, b);
    return 0;
}

void sumar(int *a, int *b) {
    *a = *a + 1;
    *b = *b + 10;
}