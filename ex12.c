// Estructuras e include
#include <stdio.h>
#include <string.h>

int main(void) {
    struct usuario {
        int edad;
        char *nombre;
        char *apellido;
    };

    struct usuario u1;

    u1.edad = 10;
    u1.nombre = "rene";
    u1.apellido = "mnqueros";

    printf("%s %s", u1.nombre, u1.apellido);

    struct otro_usuario {
        int edad;
        char nombre[100];
        char apellido[100];
    };
    struct otro_usuario u2;

    u2.edad = 20;
    strcpy(u2.nombre, "el nombre");
    strcpy(u2.apellido, "el apellido");
    printf("%s %s", u2.nombre, u2.apellido);

    return 0;
}

