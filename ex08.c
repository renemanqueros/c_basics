// ciclos
#include <stdio.h>

main() {
    int array_numeros[] = {10, 20};
    for (int i = 0; i < 2; i++) {
        printf("%d\n", array_numeros[i]);
    }

    int array_numeros_otro[] = {10, 20, 30, 40, 50};
    int tope = sizeof array_numeros_otro / sizeof(int);
    for (int i = 0; i < tope; i++) {
        printf("tope %d %d\n", tope, array_numeros_otro[i]);
    }

    int j = 0;
    while (j < 10) {
        printf("j es: %d\n", ++j);
    }

    int k = 0;
    do {
        printf("Hola");
    } while (k++ < 10);
}