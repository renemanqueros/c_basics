// Operaciones con numeros
#include <stdio.h>

main() {
    int numero = 10;
    long numerote = 200;
    float decimal = 3.3;
    double decimalote = 400.400;

    printf("%d %ld ", numero, numerote);
    printf("%f %lf", decimal, decimalote);

    numero = numero + 20;
    printf("\n%d", numero);

    int numero_nuevo = numero + 30;
    printf("\n%d", numero_nuevo);

    float conversion = numero_nuevo + .5;
    printf("\n%f", conversion);

    float numerador = 4;
    float denominador = 2;
    float resultado = numerador / denominador;
    printf("\n%f", resultado);

    numerador = 4;
    denominador = 3;
    resultado = numerador / denominador;
    printf("\n%f", resultado);

    numerador = 4;
    denominador = 3;
    resultado = (int) numerador / (int) denominador;
    printf("\n%f", resultado);
}