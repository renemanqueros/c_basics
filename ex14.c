// Archivos
#include <stdio.h>

int main(void) {
    FILE *file_pointer;
    file_pointer = fopen("bd.txt", "a");
    fprintf(file_pointer, "Hello DB\n");
    fclose(file_pointer);

    FILE *file_pointer_read;
    file_pointer_read = fopen("bd.txt", "r");
    char mensaje[2000];

    fscanf(file_pointer_read, "%[^z]", &mensaje);
    puts(mensaje);
    return 0;
}

