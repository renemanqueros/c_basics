// conficionales
#include <stdio.h>

main(){
     int numero = 10;
     int otro_numero = 103;
     if(numero > otro_numero) {
         printf("Es mayor!\n");
     } else if (numero == otro_numero) {
         printf("son iguales\n");
     } else {
         printf("Es menor!\n");
     }

     int opcion = 3;
     switch(opcion) {
         case 1:
             printf("uno");
             break;
         case 2:
             printf("dos");
             break;
         case 3:
             printf("tres");
             break;
         case 4:
             printf("cuatro");
             break;
     }
}