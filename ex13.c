// Estructuras parte 2
#include <stdio.h>

int main(void) {
    struct usuario {
        int edad;
        char *nombre;
        char *apellido;
    };

    struct usuario u1;

    u1.edad = 10;
    u1.nombre = "rene";
    u1.apellido = "manqueros";

    printf("\n%s %s", u1.nombre, u1.apellido);

    struct usuario usuarios[10];

    usuarios[20] = u1;

    printf("\n%s %s", usuarios[20].nombre, usuarios[20].apellido);

    struct usuario *apuntador_usuario = &u1;
    printf("\n%s", apuntador_usuario->nombre);

    return 0;
}

