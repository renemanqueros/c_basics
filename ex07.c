// Arreglos
#include <stdio.h>

main() {
    int array_numeros[] = {10, 20};
    printf("%d ", array_numeros[0]);
    printf("%d ", array_numeros[1]);
    printf("%d ", array_numeros[2]);
    printf("%d ", array_numeros[3]);

    float array_decimales[] = {1.1, 2.2};

    printf("\n%f %f", array_decimales[0], array_decimales[1]);

    array_numeros[3] = 30;
    printf("\n%d ", array_numeros[3]);

    float array_mixto[] = {10, 10.2};
    printf("\nMIXTO!%f %f ", array_mixto[0], array_mixto[1]);

    int array_fijo[2] = {1, 2, 3, 4};
    printf("\nFijo!%d %d %d %d ", array_fijo[0], array_fijo[1], array_fijo[2], array_fijo[3]);
}