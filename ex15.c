// Serializacion
#include <stdio.h>

int main(void) {
    struct usuario {
        int edad;
        char *nombre;
    };

    struct usuario Usuario01;
    Usuario01.nombre = "rene";
    Usuario01.edad = 35;

    FILE *bd2ptr;
    bd2ptr = fopen("data.bin", "wb");
    fwrite(&Usuario01, sizeof(struct usuario), 1, bd2ptr);
    fclose(bd2ptr);

    struct usuario Usuario02;
    FILE *bd2ptrRO;
    bd2ptrRO = fopen("data.bin", "rb");
    fread(&Usuario02, sizeof(struct usuario), 1, bd2ptrRO);
    fclose(bd2ptrRO);
    printf("\n%s %d", Usuario02.nombre, Usuario01.edad);

    FILE *bdFails;

    if ((bdFails = fopen("noexiste", "r")) == NULL) {
        puts("No existe!");
    }

    return 0;
}

