// Cadenas
#include <stdio.h>


int main(void) {
    char mensaje[] = "hola";
    puts(mensaje);

    char entrada[100];

    // gets(entrada);
    fgets(entrada, 100, stdin);
    scanf("%s", entrada);
    puts(entrada);

    char algo[2000];
    sprintf(algo, "escribiste: %s", entrada);
    printf("%s", algo);
    // restar 48
    return 0;
}

