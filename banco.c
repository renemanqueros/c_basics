#include <stdio.h>
#include <string.h>

struct Client {
    char name[100];
    char id[100];
    float funds;
};

void Balance();
void Deposit();
void Withdraw();
void Add();
void SaveClient(struct Client *selected_client);
struct Client Search();

int main(void) {
    char selected_option;

    do {
        printf("\nElija una opcion:\n");
        printf("1 - Depositar\n");
        printf("2 - Retirar\n");
        printf("3 - Agregar cliente\n");
        printf("4 - Consultar saldo\n\n");
        printf("0 - Salir\n");

        scanf("%s", &selected_option);
        selected_option = selected_option - 48;

        switch (selected_option) {
            case 1:
                Deposit();
                break;
            case 2:
                Withdraw();
                break;
            case 3:
                Add();
                break;
            case 4:
                Balance();
                break;
            case 0:
                return 0;
        }
    } while (1);
}

void Balance(){
    struct Client selected_client;
    selected_client = Search();
    printf("** Saldo disponible: %f\n", selected_client.funds);
}

void Deposit() {
    struct Client selected_client;
    float amount;
    selected_client = Search();
    printf("Monto a depositar: ");
    scanf("%f", &amount);
    selected_client.funds += amount;
    SaveClient(&selected_client);
}

void Withdraw() {
    struct Client selected_client;
    float amount;
    selected_client = Search();
    printf("Monto a retirar: ");
    scanf("%f", &amount);
    selected_client.funds -= amount;
    SaveClient(&selected_client);
}

void Add() {
    FILE *client_file;
    char id[100];
    char filename[100];
    char name[100];

    printf("\nID Cliente: ");
    scanf("%s", &id);

    printf("\nNombre: ");
    scanf("%s", &name);

    struct Client new_client;
    new_client.funds = 1000;
    strcpy(new_client.id, id);
    strcpy(new_client.name, name);
    SaveClient(&new_client);
}

void SaveClient(struct Client *selected_client) {
    FILE *client_file;
    char filename[100];

    sprintf(filename, "%s.bin", selected_client->id);
    client_file = fopen(filename, "wb");
    fwrite(selected_client, sizeof(struct Client), 1, client_file);
    fclose(client_file);
    puts("\nDatos guardados!\n");
}

struct Client Search() {
    FILE *client_file;
    struct Client read_client;
    char id[100];
    char filename[100];

    printf("\nID Cliente: ");
    scanf("%s", &id);

    sprintf(filename, "%s.bin", id);
    if ((client_file = fopen(filename, "rb")) == NULL) {
        puts("Cliente No Existe!");
        return read_client;
    }
    fread(&read_client, sizeof(struct Client), 1, client_file);
    fclose(client_file);
    return read_client;
}